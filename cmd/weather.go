package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

// Get and store the current forecast
func getCurrent(app *Application) {

	lat := "lat=" + app.Config.Latitude
	lon := "&lon=" + app.Config.Longitude
	units := "&units=" + app.Config.Units
	key := "&appid=" + app.Config.ApiKey
	url := "https://api.openweathermap.org/data/2.5/weather?" + lat + lon + units + key

	res, err := http.Get(url)
	if err != nil {
		log.Printf("Error getting current forecast: %v", err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Printf("Error reading response from server: %v\n", err)
	}

	err = json.Unmarshal(body, &app.Forecast)
	if err != nil {
		log.Printf("Error reading JSON from server: %v\n", err)
	}

}

// Query forecast by zip code
func getCurrentByLoc(app *Application) {

	zip := app.Config.Location
	units := "&units=" + app.Config.Units
	key := "&appid=" + app.Config.ApiKey
	url := "https://api.openweathermap.org/data/2.5/weather?q=" + zip + units + key

	res, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
	}

	err = json.Unmarshal(body, &app.Forecast)
	if err != nil {
		log.Println(err)
	}
}
