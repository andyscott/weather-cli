package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

// Displays news and calls loop with menu options
func mainMenu(app *Application) {

	fmt.Println("\n================================================")
	fmt.Printf("| Welcome to the OpenWeatherMap Client! v%s |\n", app.Version)
	fmt.Println("================================================")

	fmt.Printf("New in version %s:\n", app.Version)
	fmt.Println(" - Minor bugfixes")
	fmt.Println("New in version 1.0.0:")
	fmt.Println(" - Search by zip code")
	fmt.Println(" - Search by city")

	mainMenuLoop(app)
}

// Display main menu and get user input
func mainMenuLoop(app *Application) {
	var option string
	for option != "0" {
		fmt.Print("\nMain Menu\n---------\n\n")
		fmt.Println("1. Use current location (default)")
		fmt.Println("2. Search by zip code")
		fmt.Println("3. Search by city")
		fmt.Println("4. Advanced options (Change units, precise location, etc.)")
		fmt.Print("0. Exit\n\n")

		// Read user input
		reader := bufio.NewReader(os.Stdin)
		input, err := reader.ReadString('\n')
		if err != nil {
			log.Println(err)
		}
		option = strings.TrimSuffix(input, "\n")

		switch option {
		case "":
			getLocation(app)
			getCurrent(app)
			printWeather(app)
		case "1":
			getLocation(app)
			getCurrent(app)
			printWeather(app)
		case "2":
			getZip(app)
			getCurrentByLoc(app)
			printWeather(app)
		case "3":
			getCity(app)
			getCurrentByLoc(app)
			printWeather(app)
		case "4":
			advancedMenu(app)
		case "0":
			return
		default:
			fmt.Print("\nOops! An error occurred, please choose a valid option.\n\n")
		}
	}
}
