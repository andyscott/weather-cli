package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

// Display advanced menu and get user input
func advancedMenu(app *Application) {
	var option string
	// Menu loop
	for option != "0" {

		fmt.Print("\nAdvanced Menu\n-------------\n\n")
		fmt.Printf("1. Change units (Default: imperial; Current: %s)\n", app.Config.Units)
		fmt.Println("2. Enter precise location")
		fmt.Println("3. Get historical data")
		fmt.Print("0. Back\n\n")

		// Read user input
		reader := bufio.NewReader(os.Stdin)
		input, err := reader.ReadString('\n')
		if err != nil {
			log.Println(err)
		}
		option = strings.TrimSuffix(input, "\n")

		if option != "0" {
			doAdvanced(app, option)
		}
	}
}

// Perform the users selected option from the advanced menu
func doAdvanced(app *Application, option string) {
	var isValid bool
	if option == "1" {
		current := app.Config.Units
		if current == "imperial" {
			app.Config.Units = "metric"
		} else {
			app.Config.Units = "imperial"
		}
		fmt.Printf("\nChanged units from %s to %s...\n\n", current, app.Config.Units)
	} else if option == "2" {
		for !isValid {
			getPreciseLocation(app)
			getCurrent(app)
			isValid = validateCurrent(app)
		}
		printWeather(app)
		app.Forecast.Weather.Temp = -500.00
	} else if option == "3" {
		for !isValid {
			getPreciseLocation(app)
			getDate(app)
			getHistoricalData(app.Client, app)
			isValid = validateHistorical(app)
		}
		printHistorical(app)
		app.Forecast.Weather.Temp = -500.00
	}
}

// Validates results of queries by location
func validateCurrent(app *Application) bool {
	var isValid bool
	if app.Forecast.Weather.Temp != -500.00 {
		isValid = true
	} else {
		fmt.Println("I couldn't get any information for that location.")
		fmt.Println("Please double check the location you entered.")
	}
	return isValid
}

// Validates results of queries for historical data
func validateHistorical(app *Application) bool {
	var isValid bool
	if app.HistoricalForecast.Temp != -500.00 {
		isValid = true
	} else {
		fmt.Println("I couldn't get any information for that location and date.")
		fmt.Println("Please double check the information you entered.")
	}
	return isValid
}
