package main

import "fmt"

// Prints saved weather data to the terminal
func printWeather(app *Application) {

	var unitStr string
	if app.Config.Units == "imperial" {
		unitStr = "Farhenheit/mph"
	} else {
		unitStr = "Celsius/mps"
	}

	fmt.Println("\nNote! A value of 0 or 0.00 indicates the data is not available at this time.")
	fmt.Printf("Units: %s (%s)\n\n", app.Config.Units, unitStr)
	fmt.Printf("Current temperature: %.2f\n", app.Forecast.Weather.Temp)
	fmt.Printf("Feels like: %.2f\n", app.Forecast.Weather.FeelsLike)
	fmt.Printf("High: %.2f\n", app.Forecast.Weather.HighTemp)
	fmt.Printf("Low: %.2f\n", app.Forecast.Weather.LowTemp)
	fmt.Printf("Pressure (hPa): %d\n", app.Forecast.Weather.Pressure)
	fmt.Printf("Humidity (%%): %d\n", app.Forecast.Weather.Humidity)
	fmt.Printf("Wind speed: %.2f\n", app.Forecast.Wind.Speed)
	fmt.Printf("Gust: %.2f\n", app.Forecast.Wind.Gust)
}

// Prints saved historical data to the terminal
func printHistorical(app *Application) {

	var unitStr string
	if app.Config.Units == "imperial" {
		unitStr = "Farhenheit/mph"
	} else {
		unitStr = "Celsius/mps"
	}

	fmt.Println("\nNote! A value of 0 or 0.00 indicates the data is not available at this time.")
	fmt.Printf("Units: %s (%s)\n\n", app.Config.Units, unitStr)
	fmt.Printf("Temperature: %.2f\n", app.HistoricalForecast.Temp)
	fmt.Printf("Felt like: %.2f\n", app.HistoricalForecast.FeelsLike)
	fmt.Printf("Pressure (hPa): %d\n", app.HistoricalForecast.Pressure)
	fmt.Printf("Humidity (%%): %d\n", app.HistoricalForecast.Humidity)
	fmt.Printf("Wind speed: %.2f\n", app.HistoricalForecast.Speed)
}
